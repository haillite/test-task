<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class DeviceState extends Eloquent
{
    protected $fillable = ['device_id', 'state'];
    public $timestamps = false;
}
