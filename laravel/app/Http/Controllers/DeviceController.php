<?php

namespace App\Http\Controllers;

use App\Device;
use App\DeviceState;
use App\UsersDevice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DeviceController extends Controller
{
    public function index()
    {
        $userDevices = UsersDevice::where('user_id', Auth::id())->with('device')->get();
        return response($userDevices, 200);
    }
    public function store(Request $request)
    {
        $data = $request->validate([
            'login' => 'required',
            'passwd' => 'required',
            'name' => 'required',
        ]);
        $device = Device::create($data);
        $userDevices = UsersDevice::create([
            'user_id' => Auth::id(),
            'device_id' => $device->id
        ]);
        return response($device, 201);
    }
    public function show($id)
    {
        $device = Device::where([
            'user_id'=>Auth::id(),
        ]);
        return response($device, 200);
    }
    public function showState($id)
    {
        $deviceState = DeviceState::where('device_id', $id)->get(['state']);
        return response($deviceState, 200);
    }
    public function storeSate($id, Request $request)
    {
        $state = $request->validate([
            'state' => 'boolean'
        ]);
        $deviceState = DeviceState::where('device_id', $id)->update([
            'state' => $state['state']
        ]);
        if (!$deviceState) {
            $deviceState = new DeviceState();
            $deviceState->device_id = $id;
            $deviceState->state = $state['state'];
            $deviceState->save();
        }
        return response($deviceState, 200);
    }
}
