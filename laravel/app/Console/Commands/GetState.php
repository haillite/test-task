<?php

namespace App\Console\Commands;

use App\DeviceState;
use Illuminate\Console\Command;

class GetState extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:stateDevices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get state devices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(DeviceState::all(['device_id','state']));
    }
}
