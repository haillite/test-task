<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class UsersDevice extends Eloquent
{
    protected $fillable = ['user_id', 'device_id'];
    public $timestamps = false;

    public function device()
    {
        return $this->hasMany(Device::class, '_id', 'device_id');
    }
}
