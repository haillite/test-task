<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Device extends Eloquent
{
    protected $fillable = ['login', 'passwd', 'name'];
    public $timestamps = false;


}
